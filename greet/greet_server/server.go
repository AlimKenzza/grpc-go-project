package main

import (
	"fmt"
	greet "gitlab.com/AlimKenzza/grpc-go-course/greet/greetpb"
	"google.golang.org/grpc"
	"log"
	"net"
)

type CalculatorServer struct {
	greet.UnimplementedCalculatorServiceServer
}

func (s *CalculatorServer) Calculator(req *greet.CalculatorRequest, stream greet.CalculatorService_CalculatorServer) error {
	fmt.Printf("Calculator function was invoked with %v \n", req)
	number := req.GetPrimeNumber()
	primeNumbers := calculate(number)
	for i := 0; i < len(primeNumbers); i++ {
		res := &greet.CalculatorResponse{Result: primeNumbers[i]}
		if err := stream.Send(res); err != nil {
			log.Fatalf("error: %v", err.Error())
		}
	}
	return nil
	//firstName := req.GetGreeting().GetFirstName()
	//result := "Hello "  + firstName
	//res := &greet.GreetResponse{
	//	Result: result,
	//}
	//return res,nil
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	greet.RegisterCalculatorServiceServer(s, &CalculatorServer{})
	if err := s.Serve(l); err != nil {
		log.Fatalf("Failed to serve:%v", err)
	}
}

func calculate(x int32) []int32 {
	arrayOfNumbers := []int32{}
	for {
		if x%2 != 0 {
			break
		}
		arrayOfNumbers = append(arrayOfNumbers, 2)
		x /= 2
	}
	var i int32 = 0
	if x > 2 {
		arrayOfNumbers = append(arrayOfNumbers, x)
	}
	for i = 3; i <= x*x; i += 2 {
		for {
			if x%i != 0 {
				break
			}
			arrayOfNumbers = append(arrayOfNumbers, i)
			x /= i
		}
	}
	return arrayOfNumbers
}

func (s *CalculatorServer) Average(stream greet.CalculatorService_AverageServer) error {
	var counter int32 = 0
	var sum int32 = 0
	response := &greet.AverageResponse{Result: float64(sum / counter)}

}
