package main

import (
	"context"
	"fmt"
	greet "gitlab.com/AlimKenzza/grpc-go-course/greet/greetpb"
	"google.golang.org/grpc"
	"log"
)

func main() {
	fmt.Println("Hello, I am a client")
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()
	c := greet.NewCalculatorServiceClient(conn)
	PrintPrimeResponses(50,c)
}

func PrintPrimeResponses(n int32, c greet.CalculatorServiceClient) {
	req := &greet.CalculatorRequest{PrimeNumber: n}
	stream, err := c.Calculator(context.Background(), req)

	if err != nil {
		log.Fatalf("error with server stream RPC %v", err)
	}
	defer stream.CloseSend()
}

